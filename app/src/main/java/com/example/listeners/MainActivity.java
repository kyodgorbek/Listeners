package com.example.listeners;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
   private EditText toToastText;
   private Button clearButton;
    :
   private Button toastButton;
   private ButtonOnClickListener buttonListen
    private ButtonHintOnLongClick buttonHintListen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toToastText = (EditText) findViewById(R.id.etToToast);
        toToastText.setOnClickListener(new TextOnClickListener());
        clearButton = (Button) findViewById(R.id.btnClear);
        toastButton = (Button) findViewById(R.id.btnToast);
    }

    private class TextOnClickListener implements View.OnClickListener {


        @Override
        public void onClick(View v) {
            buttonListen = new ButtonOnClickListener();
            buttonHintListen = new ButtonHintOnLongClick();
            clearButton.setOnClickListener(buttonListen);
            toastButton.setOnClickListener(buttonListen);

            clearButton.setOnClickListener(buttonHintListen);
            toastButton.setOnClickListener(buttonHintListen);


        }

    }

   private class ButtonOnClickListener implements View.OnClickListener {
      @Override
       public void OnClick(View v) {
         if (v.getId() == R.id.btnClear) {
             toToastText.setText(" ");
         } else if (v.getId() == R.id.btnToast) {
             Toast.makeText(v.getContext(), toToastText.getText().toString(),
                    Toast.LENGTH_LONG).show();
         }
      }
   }

   private class ButtonHintOnLongClick implements View .OnClickListener{
   // return true so that it will consume the event and
  //the onclick will not receive a callback afterwards
       @Override
       public boolean onLongClick(View v){
           String hint = null;
           Context context = v.getContext();
           if(v.getId() == R.id.btnClear){
               hint = "This button will clear the text in the above text field";
           } else if (v.getId() == R.id.btnToast {
               hint = "This button will Toast the text in the above text field";
           }
           Toast.makeText(context, hint,
                   Toast.LENGTH_LONG).show();
           return true;


       }

   }
}